package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ScreenUtils;

public class MyGdxGame extends ApplicationAdapter {
	/*SpriteBatch batch;
	Texture img;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("goodlogic.jpg");
	}

	@Override
	public void render () {
		ScreenUtils.clear(1, 0, 0, 1);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}*/

	private Texture goodboi, spikes;

	private TextureRegion regionSpikes;

	private SpriteBatch batch;

	private int width, height;

	private float widthJoe, heightJoe;

	@Override
	public void create() {
		goodboi = new Texture("goodlogic.jpg");
		spikes = new Texture("spikes_mask.png");
		batch = new SpriteBatch();

		regionSpikes = new TextureRegion(spikes, 0, 64, 128, 64);

		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();

		widthJoe = goodboi.getWidth();
		heightJoe = goodboi.getHeight();
	}

	@Override
	public void dispose() {
		goodboi.dispose();
		batch.dispose();
		spikes.dispose();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0.5f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		// Yuhh what's rendering
		batch.draw(goodboi, 50, 0);
		batch.draw(regionSpikes, 250, 0);
		batch.end();
	}
}
